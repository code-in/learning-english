package com.ulger.learningenglish.entity.oxford;

import javax.persistence.*;

@Entity(name = "OXFORD_PACK")
public class OxfordPack {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String englishWord;
    private String turkishWord;
    private String pronunciation;
    private String color;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEnglishWord() {
        return englishWord;
    }

    public void setEnglishWord(String englishWord) {
        this.englishWord = englishWord;
    }

    public String getTurkishWord() {
        return turkishWord;
    }

    public void setTurkishWord(String turkishWord) {
        this.turkishWord = turkishWord;
    }

    public String getPronunciation() {
        return pronunciation;
    }

    public void setPronunciation(String pronunciation) {
        this.pronunciation = pronunciation;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}