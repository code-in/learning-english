package com.ulger.learningenglish.entity.oxford;

import com.ulger.learningenglish.entity.learningpack.LearningPack;
import com.ulger.learningenglish.rest.OxfordPackRepository;
import org.apache.commons.collections.IteratorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Stream;

@Service
public class OxfordPackManager {

	@Autowired
	private OxfordPackRepository oxfordPackRepository;

	@SuppressWarnings("unchecked")
	public Collection<OxfordPack> getPacks() {
		return IteratorUtils.toList(oxfordPackRepository.findAll().iterator());
	}
	
	public void save(OxfordPack oxfordPack) {
		oxfordPackRepository.save(oxfordPack);
	}
	
	public void delete(Integer id) {
		oxfordPackRepository.deleteById(id);
	}
	
	public void savePacks(Collection<LearningPack> packs) {
		packs
			.stream()
			.forEach(x -> x.setId(null));
	}
	
	public void savePacks(OxfordPack[] packs) {
		Stream
			.of(packs)
			.forEach(x -> x.setId(null));
		
		oxfordPackRepository.saveAll(Arrays.asList(packs));
	}
}