package com.ulger.learningenglish.entity.options;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OptionsManager {

	@Autowired
	private OptionsRepository optionsRepository;
	
	public Options getOptions() {
		return null;
	}
	
	public void saveOptions(Options options) {
		optionsRepository.save(options);
	}
}