package com.ulger.learningenglish.entity.learningpack;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

@Entity(name = "LEARNING_PACK")
public class LearningPack {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String englishWord;
	private String turkishWord;
	private String pronunciation;
	
	@Lob
	private String englishSentence;

	@Lob
	private String turkishSentence;

	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getEnglishWord() {
		return englishWord;
	}

	public void setEnglishWord(String englishWord) {
		this.englishWord = englishWord;
	}

	public String getTurkishWord() {
		return turkishWord;
	}

	public void setTurkishWord(String turkishWord) {
		this.turkishWord = turkishWord;
	}
	
	public String getPronunciation() {
		return pronunciation;
	}
	
	public void setPronunciation(String pronunciation) {
		this.pronunciation = pronunciation;
	}

	public String getEnglishSentence() {
		return englishSentence;
	}

	public void setEnglishSentence(String englishSentence) {
		this.englishSentence = englishSentence;
	}

	public String getTurkishSentence() {
		return turkishSentence;
	}

	public void setTurkishSentence(String turkishSentence) {
		this.turkishSentence = turkishSentence;
	}
}