package com.ulger.learningenglish.entity.learningpack;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LearningPackRepository extends CrudRepository<LearningPack, Integer> {
	
}