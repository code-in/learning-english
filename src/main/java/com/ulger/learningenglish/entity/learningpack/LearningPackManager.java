package com.ulger.learningenglish.entity.learningpack;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Stream;

import org.apache.commons.collections.IteratorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LearningPackManager {

	@Autowired
	private LearningPackRepository learningPackRepository;

	@SuppressWarnings("unchecked")
	public Collection<LearningPack> getPacks() {
		return IteratorUtils.toList(learningPackRepository.findAll().iterator());
	}
	
	public void save(LearningPack learningPack) {
		learningPackRepository.save(learningPack);
	}
	
	public void delete(Integer id) {
		learningPackRepository.deleteById(id);
	}
	
	public void savePacks(Collection<LearningPack> packs) {
		packs
			.stream()
			.forEach(x -> x.setId(null));
	}
	
	public void savePacks(LearningPack[] packs) {
		Stream
			.of(packs)
			.forEach(x -> x.setId(null));
		
		learningPackRepository.saveAll(Arrays.asList(packs));
	}
}