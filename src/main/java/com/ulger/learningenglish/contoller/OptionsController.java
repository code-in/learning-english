package com.ulger.learningenglish.contoller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ulger.learningenglish.entity.options.Options;
import com.ulger.learningenglish.entity.options.OptionsManager;

@Controller
@RequestMapping("/options")
public class OptionsController {

	@Autowired
	private OptionsManager optionsManager;
	
	@GetMapping
	public ModelAndView main() {
		return null;
	}
	
	@PostMapping
	public void update(Options options) {
		optionsManager.saveOptions(options);
	}
}