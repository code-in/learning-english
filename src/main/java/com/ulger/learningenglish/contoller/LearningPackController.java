package com.ulger.learningenglish.contoller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ulger.learningenglish.entity.learningpack.LearningPackManager;

@Controller
@RequestMapping("/")
public class LearningPackController {

	@Autowired
	private LearningPackManager learningPackManager;
	
	@GetMapping
	public ModelAndView main() {
		ModelAndView modelAndView = new ModelAndView("learning_pack");
		modelAndView.addObject("packs", learningPackManager.getPacks());
		return modelAndView;
	}
}