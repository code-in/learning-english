package com.ulger.learningenglish.contoller;

import com.ulger.learningenglish.entity.oxford.OxfordPackManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/oxford")
public class OxfordPackController {

	@Autowired
	private OxfordPackManager oxfordPackManager;

	@GetMapping
	public ModelAndView main() {
		ModelAndView modelAndView = new ModelAndView("oxford_pack");
		modelAndView.addObject("packs", oxfordPackManager.getPacks());
		return modelAndView;
	}
}