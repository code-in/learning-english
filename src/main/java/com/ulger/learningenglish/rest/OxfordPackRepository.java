package com.ulger.learningenglish.rest;

import com.ulger.learningenglish.entity.oxford.OxfordPack;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OxfordPackRepository extends CrudRepository<OxfordPack, Integer> {
	
}