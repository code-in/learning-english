package com.ulger.learningenglish.rest;

import java.io.IOException;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ulger.learningenglish.entity.learningpack.LearningPack;
import com.ulger.learningenglish.entity.learningpack.LearningPackManager;

@RestController
@RequestMapping("/learningpack")
public class LearningPackRestContoller {

	@Autowired
	private LearningPackManager learningPackManager;

	private ObjectMapper objectMapper = new ObjectMapper();
	
	@PostMapping("/update")
	public void update(LearningPack learningPack) {
		learningPackManager.save(learningPack);
	}

	@PostMapping("/delete")
	public void delete(@RequestParam("id") Integer id) {
		learningPackManager.delete(id);
	}

	@GetMapping(value = "/exportjson", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Collection<LearningPack>> exportToJson() {
		return ResponseEntity.ok(learningPackManager.getPacks());
	}
	
	@PostMapping(value = "/importjson", consumes = {"multipart/form-data"})
	public void importFromJson(@RequestPart("file") MultipartFile file) throws IOException {
		learningPackManager.savePacks(objectMapper.readValue(file.getInputStream(), LearningPack[].class));
	}
}