package com.ulger.learningenglish.rest;

import com.ulger.learningenglish.entity.oxford.OxfordPack;
import com.ulger.learningenglish.entity.oxford.OxfordPackManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/oxfordpack")
public class OxfordRestController {

    @Autowired
    private OxfordPackManager oxfordPackManager;

    @PostMapping("/update")
    public void update(OxfordPack oxfordPack) {
        oxfordPackManager.save(oxfordPack);
    }
}